package br.com.bicicletario.apiequipamento;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.bicicletario.apiequipamento.dtos.ExceptionDTO;
import jakarta.persistence.EntityNotFoundException;

@RestControllerAdvice
public class ControllerExceptionHandler {
    
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> threatDuplicateEntry(DataIntegrityViolationException exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO("Bicicleta já cadastrada", "400");
        return ResponseEntity.badRequest().body(exceptionDTO);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> threat404(EntityNotFoundException exception) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(Exception.class)
     public ResponseEntity<?> threatGeneralException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(exception.getMessage(), "500");
        return ResponseEntity.internalServerError().body(exceptionDTO);
    }

    @ExceptionHandler(IllegalArgumentException.class)
     public ResponseEntity<?> threatGeneralException(IllegalArgumentException exception) {
        if (exception.getMessage().contains("No enum constant br.com.bicicletario.apiequipamento.model.BicicletaStatus")){
            ExceptionDTO exceptionDTO = new ExceptionDTO("Status para bicicleta inválido", "500");
            return ResponseEntity.internalServerError().body(exceptionDTO);
        }
        else {
            ExceptionDTO exceptionDTO = new ExceptionDTO(exception.getMessage(), "500");
            return ResponseEntity.internalServerError().body(exceptionDTO);
        }
    }
}
