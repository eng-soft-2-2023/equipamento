package br.com.bicicletario.apiequipamento.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.bicicletario.apiequipamento.dtos.ExceptionDTO;
import br.com.bicicletario.apiequipamento.model.Bicicleta;
import br.com.bicicletario.apiequipamento.service.BicicletaService;
import jakarta.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/bicicleta")
public class BicicletaController {

    @Autowired
    private BicicletaService bicicletaService;

    @GetMapping("/hello")
    public String message(){
        return "Hello bicicleta";
    }

    /////////NOVA IMPLEMENTACAO
    // GET /bicicleta
    @GetMapping
    public ResponseEntity<List<Bicicleta>> getAllBicicletas() {
        List<Bicicleta> bicicletas = this.bicicletaService.getAllBicicletas();
        return new ResponseEntity<>(bicicletas, HttpStatus.OK);
    }

    // GET /bicicleta/{idBicicleta}
    @GetMapping("/{idBicicleta}")
    public ResponseEntity<Object> getBicicletaById (@PathVariable Long idBicicleta) {
        try {
            Bicicleta obj = this.bicicletaService.findById(idBicicleta);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Bicicleta não encontrada");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
        }
    }

    // POST /bicicleta
    @PostMapping
    public ResponseEntity<Object> createBicicleta(@RequestBody Bicicleta obj) {
        try {
            this.bicicletaService.create(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idBicicleta}").buildAndExpand(obj.getId()).toUri();
            return ResponseEntity.created(uri).body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
            return ResponseEntity.status(422).body(exceptionDTO);
        }
    }

    // POST /bicicleta/{idBicicleta}/status/{acao}
    @PostMapping("/{idBicicleta}/status/{acao}")
    public ResponseEntity<Object> updateBicicletaStatus(@PathVariable Long idBicicleta, @PathVariable String acao) {
        boolean bicicletaExists = this.bicicletaService.bicicletaExists(idBicicleta);
        try {
            Bicicleta obj = bicicletaService.updateStatus(idBicicleta, acao);
            return ResponseEntity.ok().body(obj);
        } catch (EntityNotFoundException e) {
            if (!bicicletaExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Bicicleta não encontrada");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    // POST /bicicleta/integrarNaRede
    // A CRIAR
    
    // POST /bicicleta/retirarDaRede
    // A CRIAR

    //PUT /bicicleta/{idBicicleta}
    @PutMapping("/{idBicicleta}")
    public ResponseEntity<Object> updateBicicleta(@RequestBody Bicicleta obj, @PathVariable Long idBicicleta) {
        boolean bicicletaExists = this.bicicletaService.bicicletaExists(idBicicleta);
        try {
            obj.setId(idBicicleta);
            obj = this.bicicletaService.update(obj);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            if (!bicicletaExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Bicicleta não encontrada");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    // DELETE bicicleta/{idBicicleta}
    @DeleteMapping("/{idBicicleta}")
    public ResponseEntity<Object> deleteBicicleta(@PathVariable Long idBicicleta) {
        try {
            this.bicicletaService.delete(idBicicleta);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Bicicleta não encontrada");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
        }
    }

}
