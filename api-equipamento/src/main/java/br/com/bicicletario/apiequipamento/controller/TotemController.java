package br.com.bicicletario.apiequipamento.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.bicicletario.apiequipamento.dtos.ExceptionDTO;
import br.com.bicicletario.apiequipamento.model.Bicicleta;
import br.com.bicicletario.apiequipamento.model.Totem;
import br.com.bicicletario.apiequipamento.model.Tranca;
import br.com.bicicletario.apiequipamento.service.BicicletaService;
import br.com.bicicletario.apiequipamento.service.TotemService;
import br.com.bicicletario.apiequipamento.service.TrancaService;

@RestController
@RequestMapping("totem")
public class TotemController {

    @Autowired
    private TotemService totemService;

    @Autowired
    private BicicletaService bicicletaService;

    @Autowired
    private TrancaService trancaService;


    @GetMapping("/hello")
    public String message(){
        return "Hello totem!";
    }

    // GET /totem
    @GetMapping
    public ResponseEntity<List<Totem>> getAllTotems() {
        List<Totem> totems = this.totemService.getAllTotems();
        return new ResponseEntity<>(totems, HttpStatus.OK);
    }

    // GET /totem/{idTotem}
    @GetMapping("/{idTotem}")
    public ResponseEntity<Object> getTotemById (@PathVariable Long idTotem) {
        try {
            Totem obj = this.totemService.findById(idTotem);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Totem não encontrada");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
        }
    }

    //GET /totem/{idTotem}/trancas
    @GetMapping("/{idTotem}/trancas")
    public ResponseEntity<?> getTrancasTotemId(@PathVariable Long idTotem) {
        boolean totemExists = this.totemService.existsById(idTotem);
        try {
            Totem totem = this.totemService.findById(idTotem);
            List<Tranca> trancas = this.trancaService.findTrancasByTotemLocalization(totem.getLocalizacao());
            return ResponseEntity.ok().body(trancas);
            
        } catch (Exception e) {
            if (!totemExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Totem não encontrado");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    //GET /totem/{idTotem}/bicicletas
    @GetMapping("/{idTotem}/bicicletas")
    public ResponseEntity<?> getBicicletasTotemId(@PathVariable Long idTotem) {
        boolean totemExists = this.totemService.existsById(idTotem);
        try {
            Totem totem = this.totemService.findById(idTotem);
            List<Tranca> trancas = this.trancaService.findTrancasByTotemLocalization(totem.getLocalizacao());
            List<Bicicleta> bicicletas = new ArrayList<>();
            for (Tranca tranca : trancas) {
                Bicicleta bicicleta = this.bicicletaService.findById((long)tranca.getBicicleta());
                if (bicicleta != null) {
                    bicicletas.add(bicicleta);
                }
            }
            return new ResponseEntity<>(bicicletas, HttpStatus.OK);
            
        } catch (Exception e) {
            if (!totemExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Totem não encontrado");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    // POST /totem
    @PostMapping
    public ResponseEntity<Object> createTotem(@RequestBody Totem obj) {
        try {
            this.totemService.create(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
               .path("/{idTotem}").buildAndExpand(obj.getId()).toUri();
            return ResponseEntity.created(uri).body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
            return ResponseEntity.status(422).body(exceptionDTO);
        }
    }

    // PUT /totem/{idTotem}
    @PutMapping("/{idTotem}")
    public ResponseEntity<Object> updateTotem(@RequestBody Totem obj, @PathVariable Long idTotem){
        boolean totemExists = this.totemService.existsById(idTotem);
        try {
            obj.setId(idTotem);
            obj = this.totemService.update(obj);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            if (!totemExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Totem não encontrado");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    // DELETE /totem/{idTotem}
    @DeleteMapping("/{idTotem}")
    public ResponseEntity<Object> deleteTotem(@PathVariable Long idTotem){
        try {
            this.totemService.delete(idTotem);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Totem não encontrado");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
        }
    }
}
