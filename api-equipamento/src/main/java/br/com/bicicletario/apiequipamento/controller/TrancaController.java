package br.com.bicicletario.apiequipamento.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.bicicletario.apiequipamento.dtos.ExceptionDTO;
import br.com.bicicletario.apiequipamento.dtos.IntegrarTrancaDTO;
import br.com.bicicletario.apiequipamento.model.Totem;
import br.com.bicicletario.apiequipamento.model.Tranca;
import br.com.bicicletario.apiequipamento.model.Tranca.TrancaStatus;
import br.com.bicicletario.apiequipamento.service.TotemService;
import br.com.bicicletario.apiequipamento.service.TrancaService;

@RestController
@RequestMapping("/tranca")
public class TrancaController {

    @Autowired
    private TrancaService trancaService;
    
    @Autowired
    private TotemService totemService;
    
    @GetMapping("/hello")
    public String message(){
        return "Hello tranca!";
    }

    @GetMapping
    public ResponseEntity<List<Tranca>> getAllTrancas() {
        List<Tranca> trancas = this.trancaService.getAllTrancas();
        return new ResponseEntity<List<Tranca>>(trancas, HttpStatus.OK);
    }

    @GetMapping("/{idTranca}")
    public ResponseEntity<Object> getTrancaById(@PathVariable Long idTranca) {
        try {
            Tranca obj = this.trancaService.findById(idTranca);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Tranca não encontrada");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
        }
    }

    // POST /tranca
    @PostMapping
    public ResponseEntity<Object> createTranca(@RequestBody Tranca obj) {
        try {
            this.trancaService.create(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idTranca}").buildAndExpand(obj.getId()).toUri();
            return ResponseEntity.created(uri).body(obj);
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
            return ResponseEntity.status(422).body(exceptionDTO);
        }
    }

    // PUT /tranca/{idTranca}
    @PutMapping("/{idTranca}")
    public ResponseEntity<Object> updateTranca(@RequestBody Tranca obj, @PathVariable Long idTranca) {
        boolean trancaExists = this.trancaService.existsById(idTranca);
        try {
            obj.setId(idTranca);
            obj = this.trancaService.update(obj);
            return ResponseEntity.ok().body(obj);
        } catch (Exception e) {
            if (!trancaExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Tranca não encontrada");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        } 
    }

    // DELETE tranca/{idTranca}
    @DeleteMapping("/{idTranca}")
    public ResponseEntity<Object> deleteTranca(@PathVariable Long idTranca) {
        boolean trancaExists = this.trancaService.existsById(idTranca);
        try {
            this.trancaService.delete(idTranca);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            if (!trancaExists) {
                ExceptionDTO exceptionDTO = new ExceptionDTO("404", "Tranca não encontrada");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
            } else {
                ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
            }
        }
    }

    // POST tranca/integrarNaRede
    @PostMapping("/integrarNaRede")
    public ResponseEntity<Object> integrarNaRede(@RequestBody IntegrarTrancaDTO integrarTrancaDTO) {
        try {
            Tranca tranca = this.trancaService.findById(integrarTrancaDTO.idTranca());
            Totem totem = this.totemService.findById(integrarTrancaDTO.idTotem());
    
            //incluir tranca no totem
            tranca.setLocalizacao(totem.getLocalizacao());
            tranca.setStatus(TrancaStatus.LIVRE);
            tranca = this.trancaService.update(tranca);
    
            //GET funcionario/{idFuncionario} para obter endereco de email dele
            String emailFuncionario = "fucionario1@gmail.com";
    
            // Montando corpo email
            LocalDateTime dataInsercaoTranca = LocalDateTime.now();
            int numeroTranca = tranca.getNumero();
            Long matriculaReparador = integrarTrancaDTO.idFuncionario();
    
            String emailMensagem = "Prezado(a), tranca incluida com os dados: ";
            emailMensagem = emailMensagem + "Numero da tranca: "+ numeroTranca;
            emailMensagem = emailMensagem + "Funcionario "+ matriculaReparador;
            emailMensagem = emailMensagem + "Data/Hora "+ dataInsercaoTranca;
    
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("email", emailFuncionario);
            payload.put("assunto", "Integração de Tranca na Rede");
            payload.put("mensagem", emailMensagem);
    
            //Envia email
            this.trancaService.enviarEmail(payload);
    
            return ResponseEntity.ok().build();
            
        } catch (Exception e) {
            ExceptionDTO exceptionDTO = new ExceptionDTO("422", "Dados Inválidos");
                return ResponseEntity.status(422).body(exceptionDTO);
        }
    }
}
