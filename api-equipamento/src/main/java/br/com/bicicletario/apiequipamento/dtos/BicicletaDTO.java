package br.com.bicicletario.apiequipamento.dtos;

import br.com.bicicletario.apiequipamento.model.BicicletaStatus;

public record BicicletaDTO(String marca, String modelo, String ano, int numero, BicicletaStatus status) {
    
}
