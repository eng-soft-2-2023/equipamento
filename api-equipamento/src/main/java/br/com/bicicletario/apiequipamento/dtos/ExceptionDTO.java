package br.com.bicicletario.apiequipamento.dtos;

public record ExceptionDTO(String codigo, String mensagem) {
    
}
