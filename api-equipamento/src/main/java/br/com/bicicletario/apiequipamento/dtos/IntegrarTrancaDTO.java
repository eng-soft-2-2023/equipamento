package br.com.bicicletario.apiequipamento.dtos;

public record IntegrarTrancaDTO(Long idTotem, Long idTranca, Long idFuncionario) {
    
}
