package br.com.bicicletario.apiequipamento.model;

import br.com.bicicletario.apiequipamento.dtos.BicicletaDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name="Bicicletas")
@Table(name="Bicicletas")
@EqualsAndHashCode(of="id")
public class Bicicleta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;
    
    @Column(name = "marca", nullable = false)
    private String marca;

    @Column(name = "modelo", nullable = false)
    private String modelo;

    @Column(name = "ano", nullable = false)
    private String ano;

    @Column(name = "numero", nullable = false)
    private int numero;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private BicicletaStatus status;
    
    public Bicicleta(BicicletaDTO data) {
        this.marca = data.marca();
        this.modelo = data.modelo();
        this.ano = data.ano();
        this.numero = data.numero();
        this.status = data.status();
    }
}
