package br.com.bicicletario.apiequipamento.model;

public enum BicicletaStatus {
    DISPONIVEL,
    EM_USO,
    NOVA,
    APOSENTADA,
    REPARO_SOLICITADO,
    EM_REPARO
}
