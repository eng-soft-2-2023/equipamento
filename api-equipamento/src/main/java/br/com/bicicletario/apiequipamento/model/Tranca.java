package br.com.bicicletario.apiequipamento.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name="Trancas")
@Table(name="Trancas")
@EqualsAndHashCode(of="id")
public class Tranca {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "bicicleta", nullable = false)
    private int bicicleta;
    
    @Column(name = "numero", nullable = false)
    private int numero;

    @Column(name = "localizacao", nullable = false)
    private String localizacao;

    @Column(name = "anoDeFabricacao", nullable = false)
    private String anoDeFabricacao;

    @Column(name = "modelo", nullable = false)
    private String modelo;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TrancaStatus status;

    public enum TrancaStatus {
        LIVRE,
        OCUPADA,
        NOVA,
        APOSENTADA,
        EM_REPARO
    }



}