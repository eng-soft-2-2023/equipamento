package br.com.bicicletario.apiequipamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.bicicletario.apiequipamento.model.Totem;

@Repository
public interface TotemRepository extends JpaRepository<Totem, Long>{
    public Totem findTotemByLocalizacao(String localizacao);
}
