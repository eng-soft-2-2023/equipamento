package br.com.bicicletario.apiequipamento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.bicicletario.apiequipamento.model.Tranca;

@Repository
public interface TrancaRepository extends JpaRepository<Tranca, Long>{
    public Tranca findTrancaByBicicleta(int id);

    public Tranca findTrancaByLocalizacao(String localizacao);

    @Query("select t from Trancas t where t.localizacao like %:local%")
    public List<Tranca> findTrancasByTotemLocalization(@Param("local") String local);
}