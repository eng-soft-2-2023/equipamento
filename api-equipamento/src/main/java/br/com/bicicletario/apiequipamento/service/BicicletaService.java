package br.com.bicicletario.apiequipamento.service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.bicicletario.apiequipamento.model.Bicicleta;
import br.com.bicicletario.apiequipamento.model.BicicletaStatus;
import br.com.bicicletario.apiequipamento.model.Tranca;
import br.com.bicicletario.apiequipamento.repository.BicicletaRepository;

@Service
public class BicicletaService {
    
    @Autowired
    private BicicletaRepository bicicletaRepository;

    @Autowired
    private TrancaService trancaService;

    /////////////////////////

    public boolean bicicletaExists(Long id) {
        return this.bicicletaRepository.existsById(id);
    }

    public List<Bicicleta> getAllBicicletas() {
        return this.bicicletaRepository.findAll();
    }

    ///////////// NOVA IMPLEMENTACAO
    public Bicicleta findById(Long id) {
        Optional<Bicicleta> bicicleta = this.bicicletaRepository.findById(id);
            
        return bicicleta.orElseThrow(() -> new RuntimeException("Bicicleta não encontrada"));
    }

    public boolean existsById(Long id) {
        return this.bicicletaRepository.existsById(id);
    }
    
    @Transactional
    public Bicicleta create(Bicicleta obj) {
        obj.setId(null);

        // R5
        obj.setNumero(gerarNumeroAleatorio());
        
        // R1
        obj.setStatus(BicicletaStatus.NOVA);

        obj = this.bicicletaRepository.save(obj);
        return obj;
    }

    // R5
    private int gerarNumeroAleatorio() {
        Random random = new Random();
        //Gera números aleatórios entre 1 e 999
        return random.nextInt(1000) + 1; 
    }

    @Transactional
    public Bicicleta update(Bicicleta obj) {
        Bicicleta newObj = findById(obj.getId());

        // R3: Número e status não podem ser editados
        if (obj.getMarca() != null) {
            newObj.setMarca(obj.getMarca());
        }
        if (obj.getModelo() != null) {
            newObj.setModelo(obj.getModelo());
        }
        if (obj.getAno() != null) {
            newObj.setAno(obj.getAno());
        }
        
        newObj = this.bicicletaRepository.save(newObj);

        return newObj;
    }

    @Transactional
    public Bicicleta updateStatus(Long Id, String status) {
        Bicicleta newObj = findById(Id);
        BicicletaStatus statusEnumValue = BicicletaStatus.valueOf(status.toUpperCase());
        if (status != null) {
            newObj.setStatus(statusEnumValue);
            newObj = this.bicicletaRepository.save(newObj);
        }
        return newObj;
    }

    @Transactional
    public void delete(Long id) {
        try {
            Bicicleta bicicleta = findById(id);
            BicicletaStatus status = bicicleta.getStatus();
            Tranca tranca = this.trancaService.findTrancaByBicicletaId(id.intValue());

            boolean exp1;
            boolean exp2;

            if (tranca == null) {
                exp2 = true;
            } else {
                exp2 = !(tranca.getBicicleta() == id.intValue());
            }
                
            exp1 = (status.equals(BicicletaStatus.APOSENTADA));
            
            // R4
            if(exp1 && exp2) {
                this.bicicletaRepository.deleteById(id);
            }

        } catch (Exception e) {
            throw new RuntimeException("Não é possível excluir bicicleta");
        }
    }

}
