package br.com.bicicletario.apiequipamento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.bicicletario.apiequipamento.model.Totem;
import br.com.bicicletario.apiequipamento.repository.TotemRepository;

@Service
public class TotemService {
    
    @Autowired
    private TotemRepository totemRepository;

    public Totem findById(Long id) {
        Optional<Totem> totem = this.totemRepository.findById(id);
        return totem.orElseThrow(() -> new RuntimeException("Totem não encontrado"));
    }

    public boolean existsById(Long id) {
        return this.totemRepository.existsById(id);
    }

    public List<Totem> getAllTotems() {
        return this.totemRepository.findAll();
    }

    @Transactional
    public Totem create(Totem obj) {
        obj.setId(null);
        obj = this.totemRepository.save(obj);
        return obj;
    }

    @Transactional
    public Totem update(Totem obj) {
        Totem newObj = findById(obj.getId());

        if (obj.getLocalizacao() != null) {
            newObj.setLocalizacao(obj.getLocalizacao());
        }
        if (obj.getDescricao() != null) {
            newObj.setDescricao(obj.getDescricao());
        }

        newObj = this.totemRepository.save(newObj);
        return newObj;
    }

    @Transactional
    public void delete (Long id) {
        findById(id);
        try {
            this.totemRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Não é possível excluir totem");
        }
    }

    public Totem findTotemByLocalizacao(String localizacao) {
        return this.totemRepository.findTotemByLocalizacao(localizacao);
    }
}
