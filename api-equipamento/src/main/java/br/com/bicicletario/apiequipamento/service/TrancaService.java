package br.com.bicicletario.apiequipamento.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import br.com.bicicletario.apiequipamento.model.Tranca;
import br.com.bicicletario.apiequipamento.model.Tranca.TrancaStatus;
import br.com.bicicletario.apiequipamento.repository.TrancaRepository;

@Service
public class TrancaService {
    
    @Autowired
    private TrancaRepository trancaRepository;

    @Autowired
    private RestTemplate restTemplate;

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public Map<String, Object> enviarEmail(Map<String, Object> payload){
        HttpEntity<Map> httpEntity = new HttpEntity<>(payload, getHttpHeaders());
        String url = "http://localhost:8082/enviarEmail";
        var response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Map.class);
        return response.getBody();
    }

    public Map<String, Object> getFuncionarioById(Long idFuncionario){
        HttpEntity<Void> httpEntity = new HttpEntity<>(getHttpHeaders());
        String url = "http://localhost:8081/enviarEmail/funcionario/" + idFuncionario;
        var response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class);
        return response.getBody();
    }


    public Tranca findById(Long id) {
        Optional<Tranca> tranca = this.trancaRepository.findById(id);
        return tranca.orElseThrow(() -> new RuntimeException("Tranca não encontrada"));
    }

    public boolean existsById(Long id) {
        return this.trancaRepository.existsById(id);
    }

    public List<Tranca> getAllTrancas() {
        return this.trancaRepository.findAll();
    }

    @Transactional
    public Tranca create(Tranca obj) {
        obj.setId(null);    

        // R1
        obj.setStatus(TrancaStatus.NOVA);

        obj = this.trancaRepository.save(obj);
        return obj;
    }

    @Transactional
    public Tranca update(Tranca obj) {
        Tranca newObj = findById(obj.getId());

        if (obj.getAnoDeFabricacao() != null) {
            newObj.setAnoDeFabricacao(obj.getAnoDeFabricacao());
        }
        if (obj.getBicicleta() != 0) {
            newObj.setBicicleta(obj.getBicicleta());
        }
        if (obj.getLocalizacao() != null) {
            newObj.setLocalizacao(obj.getLocalizacao());
        }
        if (obj.getModelo() != null) {
            newObj.setLocalizacao(obj.getLocalizacao());
        }

        newObj = this.trancaRepository.save(newObj);
        return newObj;
    }

    @Transactional
    public void delete(Long id) {
        Tranca obj = findById(id);
        try {
            if (obj.getBicicleta() == 0)
                this.trancaRepository.deleteById(id);
            else 
                throw new RuntimeException("Existe uma bicicleta na tranca");
        } catch (Exception e) {
            throw new RuntimeException("Não é possível excluir tranca");
        }
    }

    public Tranca findTrancaByBicicletaId(int id) {
        return this.trancaRepository.findTrancaByBicicleta(id);
    }

    public Tranca findTrancaByLocalizacao(String localizacao) {
        return this.trancaRepository.findTrancaByLocalizacao(localizacao);
    }

    public List<Tranca> findTrancasByTotemLocalization(String localizacao) {
        return this.trancaRepository.findTrancasByTotemLocalization(localizacao);
    }
}
