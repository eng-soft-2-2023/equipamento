package br.com.bicicletario.apiequipamento.repository;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.assertj.core.api.Assertions.assertThat;

import br.com.bicicletario.apiequipamento.dtos.BicicletaDTO;
import br.com.bicicletario.apiequipamento.model.Bicicleta;
import br.com.bicicletario.apiequipamento.model.BicicletaStatus;
import jakarta.persistence.EntityManager;

@DataJpaTest
@ActiveProfiles("test")
public class BicicletaRepositoryTest {

    @Autowired
    BicicletaRepository bicicletaRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    @DisplayName("Should get Bicicleta sucessfully from DB.")
    void testFindBicicletaByIdSucess() {
        BicicletaDTO data = new BicicletaDTO("marca 1", "modelo 1", "1991", 1111, BicicletaStatus.NOVA);
        this.createBicicleta(data);
        
        Optional<Bicicleta> result = this.bicicletaRepository.findById(1L);

        assertThat(result.isPresent()).isTrue();
    }

    @Test
    @DisplayName("Should not get Bicicleta from DB when user not exists.")
    void testFindBicicletaByIdFail() {
        Optional<Bicicleta> result = this.bicicletaRepository.findById(1L);

        assertThat(result.isEmpty()).isTrue();
    }

    private Bicicleta createBicicleta(BicicletaDTO data) {
        Bicicleta newBicicleta = new Bicicleta(data);
        this.entityManager.persist(newBicicleta);
        return newBicicleta;
    }



}
