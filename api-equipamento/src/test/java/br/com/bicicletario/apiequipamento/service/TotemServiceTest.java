package br.com.bicicletario.apiequipamento.service;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.bicicletario.apiequipamento.model.Totem;
import br.com.bicicletario.apiequipamento.repository.TotemRepository;

public class TotemServiceTest {

    @Mock
    private TotemRepository totemRepository;

    @InjectMocks
    private TotemService totemService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testFindById() {
        // Arrange
        Long totemId = 1L;
        Totem mockTotem = new Totem();
        mockTotem.setId(totemId);
        when(totemRepository.findById(totemId)).thenReturn(Optional.of(mockTotem));

        // Act
        Totem result = totemService.findById(totemId);

        // Assert
        assertNotNull(result);
        assertEquals(totemId, result.getId());
    }

    @Test
    void testFindByIdNotFound() {
        // Arrange
        Long totemId = 1L;
        when(totemRepository.findById(totemId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(RuntimeException.class, () -> totemService.findById(totemId));
    }

    @Test
    void testExistsById() {
        // Arrange
        Long totemId = 1L;
        when(totemRepository.existsById(totemId)).thenReturn(true);

        // Act
        boolean result = totemService.existsById(totemId);

        // Assert
        assertTrue(result);
    }

    @Test
    void testGetAllTotems() {
        // Arrange
        List<Totem> mockTotems = new ArrayList<>();
        mockTotems.add(new Totem());
        when(totemRepository.findAll()).thenReturn(mockTotems);

        // Act
        List<Totem> result = totemService.getAllTotems();

        // Assert
        assertNotNull(result);
        assertEquals(mockTotems.size(), result.size());
    }

    // Adicione mais testes para os métodos create, update, delete, findTotemByLocalizacao, etc.

    @Test
    void testCreate() {
        // Arrange
        Totem totemToCreate = new Totem();
        when(totemRepository.save(totemToCreate)).thenReturn(totemToCreate);

        // Act
        Totem result = totemService.create(totemToCreate);
        result.setId(1L);

        // Assert
        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    void testUpdate() {
        // Arrange
        Long totemId = 1L;
        Totem existingTotem = new Totem();
        existingTotem.setId(totemId);
        when(totemRepository.findById(totemId)).thenReturn(Optional.of(existingTotem));

        Totem updatedTotem = new Totem();
        updatedTotem.setId(totemId);
        updatedTotem.setDescricao("Nova descrição");

        when(totemRepository.save(updatedTotem)).thenReturn(updatedTotem);

        // Act
        Totem result = totemService.update(updatedTotem);

        // Assert
        assertNotNull(result);
        assertEquals(updatedTotem.getDescricao(), result.getDescricao());
    }

    @Test
    void testDelete() {
        // Arrange
        Long totemId = 1L;
        Totem existingTotem = new Totem();
        existingTotem.setId(totemId);
        when(totemRepository.findById(totemId)).thenReturn(Optional.of(existingTotem));

        // Act
        assertDoesNotThrow(() -> totemService.delete(totemId));

        // Assert
        verify(totemRepository, times(1)).deleteById(totemId);
    }

    @Test
    void testFindTotemByLocalizacao() {
        // Arrange
        String localizacao = "Algum lugar";
        Totem mockTotem = new Totem();
        when(totemRepository.findTotemByLocalizacao(localizacao)).thenReturn(mockTotem);

        // Act
        Totem result = totemService.findTotemByLocalizacao(localizacao);

        // Assert
        assertNotNull(result);
    }
}
