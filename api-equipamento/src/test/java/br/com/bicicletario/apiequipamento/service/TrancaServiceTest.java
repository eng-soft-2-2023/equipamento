package br.com.bicicletario.apiequipamento.service;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.*;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import br.com.bicicletario.apiequipamento.model.Tranca;
import br.com.bicicletario.apiequipamento.repository.TrancaRepository;

@ExtendWith(MockitoExtension.class)
class TrancaServiceTest {

    @Mock
    private TrancaRepository trancaRepository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private TrancaService trancaService;

    @Test
    void testEnviarEmail() {
        // Mocking
        Map<String, Object> payload = new HashMap<>();
        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class), eq(Map.class)))
                .thenReturn(new ResponseEntity<>(payload, HttpStatus.OK));

        // Test
        Map<String, Object> result = trancaService.enviarEmail(payload);

        // Verify
        assertNotNull(result);
    }

    @Test
    void testGetFuncionarioById() {
        // Mocking
        Long idFuncionario = 1L;
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(Map.class)))
                .thenReturn(new ResponseEntity<>(Collections.singletonMap("id", idFuncionario), HttpStatus.OK));

        // Test
        Map<String, Object> result = trancaService.getFuncionarioById(idFuncionario);

        // Verify
        assertNotNull(result);
        assertEquals(idFuncionario, result.get("id"));
    }

    @Test
    void testFindById() {
        // Mocking
        Long id = 1L;
        when(trancaRepository.findById(id)).thenReturn(Optional.of(new Tranca()));

        // Test
        Tranca result = trancaService.findById(id);

        // Verify
        assertNotNull(result);
    }

    @Test
    void testExistsById() {
        // Mocking
        Long id = 1L;
        when(trancaRepository.existsById(id)).thenReturn(true);

        // Test
        boolean result = trancaService.existsById(id);

        // Verify
        assertTrue(result);
    }


    @Test
    void testDelete() {
        // Mocking
        Long id = 1L;
        Tranca tranca = new Tranca();
        tranca.setBicicleta(0);
        when(trancaRepository.findById(id)).thenReturn(Optional.of(tranca));

        // Test
        assertDoesNotThrow(() -> trancaService.delete(id));

    }
    

    @Test
    void testCreate() {
        // Mocking
        Tranca trancaToCreate = new Tranca();
        trancaToCreate.setId(1L);
        when(trancaRepository.save(any(Tranca.class))).thenReturn(trancaToCreate);

        // Test
        Tranca createdTranca = trancaService.create(new Tranca());

        // Verify
        assertNotNull(createdTranca);
        assertEquals(Tranca.TrancaStatus.NOVA, createdTranca.getStatus());
    }

    @Test
    void testUpdate() {
        // Mocking
        Long existingTrancaId = 1L;
        Tranca existingTranca = new Tranca();
        existingTranca.setId(existingTrancaId);
        existingTranca.setAnoDeFabricacao("2022");
        existingTranca.setBicicleta(123);
        existingTranca.setLocalizacao("Location A");
        existingTranca.setModelo("Model X");

        when(trancaRepository.findById(existingTrancaId)).thenReturn(Optional.of(existingTranca));
        when(trancaRepository.save(any(Tranca.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // Test
        Tranca updatedTranca = trancaService.update(existingTranca);

        // Verify
        assertNotNull(updatedTranca);
        assertEquals(existingTrancaId, updatedTranca.getId());
        assertEquals("2022", updatedTranca.getAnoDeFabricacao());
        assertEquals(123, updatedTranca.getBicicleta());
        assertEquals("Location A", updatedTranca.getLocalizacao());
        assertEquals("Model X", updatedTranca.getModelo());
    }

    @Test
    void testDeleteWithBicycle() {
        // Mocking
        Long id = 1L;
        Tranca tranca = new Tranca();
        tranca.setBicicleta(123);
        when(trancaRepository.findById(id)).thenReturn(Optional.of(tranca));

        // Test and Verify
        assertThrows(RuntimeException.class, () -> trancaService.delete(id));
    }

    @Test
    void testFindTrancaByBicicletaId() {
        // Mocking
        int bicicletaId = 123;
        when(trancaRepository.findTrancaByBicicleta(bicicletaId)).thenReturn(new Tranca());

        // Test
        Tranca result = trancaService.findTrancaByBicicletaId(bicicletaId);

        // Verify
        assertNotNull(result);
    }

    @Test
    void testFindTrancaByLocalizacao() {
        // Mocking
        String localizacao = "Location A";
        when(trancaRepository.findTrancaByLocalizacao(localizacao)).thenReturn(new Tranca());

        // Test
        Tranca result = trancaService.findTrancaByLocalizacao(localizacao);

        // Verify
        assertNotNull(result);
    }

    @Test
    void testFindTrancasByTotemLocalization() {
        // Mocking
        String totemLocalization = "Totem Location";
        when(trancaRepository.findTrancasByTotemLocalization(totemLocalization)).thenReturn(Collections.singletonList(new Tranca()));

        // Test
        List<Tranca> result = trancaService.findTrancasByTotemLocalization(totemLocalization);

        // Verify
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }
}
